FROM debian:jessie
MAINTAINER Harish Anchu <harishanchu@wimoku.com>

RUN apt-get update && \
    apt-get --no-install-recommends install -q -y git && \
    apt-get --no-install-recommends install -q -y openjdk-7-jre-headless && \
    rm -rf /var/lib/apt/lists/*
ADD http://mirrors.jenkins-ci.org/war/1.624/jenkins.war /opt/jenkins.war
RUN chmod 644 /opt/jenkins.war
ENV JENKINS_HOME /jenkins

ADD id_rsa_docker /root/.ssh/id_rsa
ADD docker_aws.pem /root/.ssh/docker_aws.pem

ENTRYPOINT ["java", "-jar", "/opt/jenkins.war"]
EXPOSE 8080
CMD [""]